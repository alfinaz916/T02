public class lcd {
    private String status;
    private int volume;
    private int brightness;
    private String cable;

    public String turnOff(){
        return status;
    }
    public String turnOn(){
        return status;
    }
    public void setStatus( String s){
    status = s;
    }
    public String Freeze(){
        return status;
    }
    public int volumeUp(){
        return volume;
    }
    public int volumeDown(){
        return volume;
    }
    public void setVolume(int i){
        volume = i;
    }
    public int brightnessUp(){
        return brightness;
    }
    public int brightnessDown(){
        return brightness;
    }
    public void setBrightness( int i){
        brightness = i;
    }
    public String cableUp(){
        return cable;
    }
    public String cableDown(){
        return cable;
    }
    public void setCable( String s){
        cable = s;
    }

    public void displayMessage(){
        System.out.println("---------------LCD---------------");
        System.out.println("Status LCD saat ini       : "+ status);
        System.out.println("Volume LCD saat ini       : "+ volume);
        System.out.println("Brightness LCD saat ini   : "+ brightness);
        System.out.println("Cable LCD saat ini        : "+ cable);
    }

}